package application.graingrowth.file;

import application.graingrowth.utils.Cell;
import application.graingrowth.utils.CellType;
import application.graingrowth.utils.Color;
import application.graingrowth.utils.Grain;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class FileImport {
    File file;

    public FileImport(File file) {
        this.file = file;
    }

    public TImport importFile(){
        String extension = "";

        int i = file.getName().lastIndexOf('.');
        if (i > 0) {
            extension = file.getName().substring(i+1);
        }

        switch (extension){
            case "json":
                System.out.println("Import form JSON");
                return importFromJson();
            case "bmp":
                System.out.println("bmp");
                return importFromBmp();
        }
        return null;
    }

    private TImport importFromJson(){
        ObjectMapper mapper = new ObjectMapper();
        TImport obj = null;
        try {
            obj = mapper.readValue(file, TImport.class);
        } catch (IOException e) {
            System.out.println("Error importFromJson");
            e.printStackTrace();
        }
        return obj;
    }

    private TImport importFromBmp(){
        BufferedImage image = null;
        try {
            image = ImageIO.read(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(image == null) return null;
        int height = image.getHeight() / 10;
        int width = image.getWidth() / 10;

        int[][] array2D = new int[height][width];

        for (int xPixel = 0; xPixel < image.getWidth(); xPixel += 10) {
            for (int yPixel = 0; yPixel < image.getHeight(); yPixel += 10) {
                array2D[yPixel/10][xPixel/10] = image.getRGB(xPixel, yPixel);
            }
        }

        Map<Integer, Grain> grainsByColor = new HashMap<>();
        Map<Integer, Grain> grainsById = new HashMap<>();
        int grainsCount = 1;

        Cell[][] cellTab = new Cell[height][width];

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                Cell cell = new Cell(x, y);
                int rgbColor = array2D[y][x];

                switch (rgbColor) {
                    case 0:
                    case -16777216:
                        cell.setType(CellType.inclusion);
                        break;
                    case 16777215:
                    case -1:
                        cell.setType(CellType.empty);
                        break;
                    default:
                        cell.setType(CellType.grain);
                        Grain grain = grainsByColor.get(rgbColor);
                        if(grain == null){
                            grain = new Grain(grainsCount, Color.createColor(array2D[y][x]));
                            grainsByColor.put(array2D[y][x], grain);
                            grainsCount++;
                            grainsById.put(grain.getId(), grain);
                        }
                        cell.setGrain(grain);
                }

                cellTab[y][x] = cell;


            }
        }
        return new TImport(cellTab, grainsById, height, width);
    }

}
