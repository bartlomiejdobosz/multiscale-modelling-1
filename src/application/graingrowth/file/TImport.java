package application.graingrowth.file;

import application.graingrowth.utils.Cell;
import application.graingrowth.utils.Grain;

import java.util.Map;

public class TImport {
    private Cell[][] cells;
    private Map<Integer, Grain> grains;
    private int height;
    private int width;

    public TImport(){}

    public TImport(Cell[][] cells, Map<Integer, Grain> grains) {
        this.cells = cells;
        this.grains = grains;
    }

    public TImport(Cell[][] cells, Map<Integer, Grain> grains, int height, int width) {
        this.cells = cells;
        this.grains = grains;
        this.height = height;
        this.width = width;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public Map<Integer, Grain> getGrains() {
        return grains;
    }

    public void setGrains(Map<Integer, Grain> grains) {
        this.grains = grains;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
