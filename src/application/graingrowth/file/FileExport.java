package application.graingrowth.file;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FileExport {
    private BufferedImage buffImg;
    private File file;

    public FileExport(File file) {
        this.file = file;
    }

    public void export(TImport tImport){
        String extension = "";

        int i = file.getName().lastIndexOf('.');
        if (i > 0) {
            extension = file.getName().substring(i+1);
        }

        switch (extension){
            case "json":
                System.out.println("Export to JSON");
                exportJson(tImport);
                break;
            case "bmp":
                System.out.println("bmp");
                exportBmp(tImport);
                break;
        }
    }

    public void exportJson(TImport tImport){
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(file, tImport);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error with json file");

        }
    }

    public void exportBmp(TImport tImport){
        buffImg = new BufferedImage(tImport.getWidth() * 10, tImport.getHeight() * 10, BufferedImage.TYPE_INT_RGB);

        for (int i = 0; i < tImport.getWidth(); i++)
            for (int j = 0; j < tImport.getHeight(); j++) {
                switch (tImport.getCells()[j][i].getType()) {
                    case grain:
                        int rgb = tImport.getCells()[j][i].getGrain().getColor().getRgb();
                        setGrainInImg(i, j, rgb);
                        break;
                    case empty:
                        setGrainInImg(i, j, 16777215);
                        break;
                    case inclusion:
                        setGrainInImg(i, j, 0);
                        break;
                }
            }
        try {
            ImageIO.write(buffImg, "BMP", file);
        } catch (IOException ex) {
            System.out.println("ex bmp: ");
        }
    }

    private void setGrainInImg(int x, int y, int color){
        for(int i = x * 10; i < (x +1) * 10; i++) {
            for(int j = y * 10; j < (y +1) * 10; j++) {
                buffImg.setRGB(i, j, color);
            }
        }
    }
}
