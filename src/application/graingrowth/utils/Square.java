package application.graingrowth.utils;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Square extends Rectangle {
    int x;
    int y;

    public Square(){
        init();
    }

    public Square(int x, int y){
        this.x = x;
        this.y = y;
        init();
    }

    private void init(){
        this.setHeight(10);
        this.setWidth(10);
        this.setFill(Color.WHITE);
    }

    public int getXcord() {
        return x;
    }

    public void setXcord(int x) {
        this.x = x;
    }

    public int getYcord() {
        return y;
    }

    public void setYcord(int y) {
        this.y = y;
    }
}
