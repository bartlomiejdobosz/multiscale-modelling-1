package application.graingrowth.utils;

public enum CellType {
    empty,
    grain,
    inclusion,
    selected
}
