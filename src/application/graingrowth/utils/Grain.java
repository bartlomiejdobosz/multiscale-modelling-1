package application.graingrowth.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Grain {
    private int id;
    private Color color;

    public Grain(int id){
        this.id = id;
        this.color = Color.randomColor();
    }
}
