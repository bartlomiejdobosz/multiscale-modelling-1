package application.main;

import application.graingrowth.GrainGrowth;
import application.main.MainController;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));

        Parent root = loader.load();

        MainController controller = loader.getController();
        controller.tabPane.getTabs().addAll(definePanels(primaryStage));

        primaryStage.setTitle("Metody Wieloskalowe");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();
    }

    private Tab[] definePanels(Stage stage) throws IOException {
        String tabs[][] = {
                {"Grain growth", "/application/graingrowth/GrainGrowth.fxml"}
        };
        Tab tab[] = new Tab[tabs.length];
        for(int i =0; i < tabs.length; i++){
            tab[i] = new Tab();
            tab[i].setText(tabs[i][0]);

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(tabs[i][1]));

            BorderPane view;
            view = fxmlLoader.load();
            GrainGrowth controller = (GrainGrowth)fxmlLoader.getController();
            controller.setStage(stage);


            tab[i].setContent(view);
        }
        return tab;
    }

    @Override
    public void stop(){
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
